
SELECT 
  item.item_id,
  item.item_name,
  item.item_description,
  item.item_price,
  item.item_popular,
  item.item_created,
  item.item_updated,
  category.category_title     
FROM "item"
INNER JOIN "category"
ON item.category_id=category.category_id;

SELECT * FROM "item"
WHERE item_id IN(
  SELECT item_id FROM "item__order"
  WHERE order_id = 1
  );

SELECT * FROM "order"
WHERE order_id IN(
  SELECT order_id FROM "item__order"
  WHERE item_id = 1
  );

SELECT * FROM "item"
WHERE item_id IN(
  SELECT item_id FROM "item__order"
  WHERE order_id IN(
    SELECT order_id FROM "order"
    WHERE order_created BETWEEN now() - interval '1 hour' AND now()
    )
  );
  
SELECT * FROM "item"
WHERE item_id IN(
  SELECT item_id FROM "item__order"
  WHERE order_id IN(
    SELECT order_id FROM "order"
    WHERE order_created BETWEEN now() - interval '1 day' AND now()
    )
  );
  
SELECT * FROM "item"
WHERE item_id IN(
  SELECT item_id FROM "item__order"
  WHERE order_id IN(
    SELECT order_id FROM "order"
    WHERE order_created BETWEEN now() - interval '2 days' AND now() - interval '1 day'
    )
  );

SELECT * FROM(
  SELECT * FROM "item"
  WHERE item_id IN(
    SELECT item_id FROM "item__order"
    WHERE order_id IN(
      SELECT order_id FROM "order"
      WHERE order_created BETWEEN now() - interval '1 hour' AND now()
      )
    )
  ) "sub"
WHERE category_id = ?;

SELECT * FROM(
  SELECT * FROM "item"
  WHERE item_id IN(
    SELECT item_id FROM "item__order"
    WHERE order_id IN(
      SELECT order_id FROM "order"
      WHERE order_created BETWEEN now() - interval '1 day' AND now()
      )
    )
  ) "sub"
WHERE category_id = ?;

SELECT * FROM(
  SELECT * FROM "item"
  WHERE item_id IN(
    SELECT item_id FROM "item__order"
    WHERE order_id IN(
      SELECT order_id FROM "order"
      WHERE order_created BETWEEN now() - interval '2 days' AND now() - interval '1 day'
      )
    )
  ) "sub"
WHERE category_id = ?;

SELECT * FROM "item"
WHERE item_name LIKE '?%';

SELECT * FROM "item"
WHERE item_name LIKE '%?';

SELECT * FROM "item"
WHERE item_name LIKE '%?%';


